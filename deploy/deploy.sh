#!/bin/bash

set -x

# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# disable the host key checking.
./deploy/disableHostKeyChecking.sh

DEPLOY_SERVERS=$DEPLOY_SERVER

ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
echo "ALL_SERVERS ${ALL_SERVERS}"

#for each ec2 instance
for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  ssh ec2-user@${server} 'bash' < ./deploy/updateAndRestart.sh
done
#!/bin/bash

# clone the repo again
git clone https://gitlab.com/leenab2205/helloworld.git

# stop the previous pm2
pm2 kill
npm remove pm2 -g

npm install pm2 -g

pm2 status

cd /home/ec2-user/helloworld

#install npm packages
echo "Running npm install"
npm install

#Restart the node server
npm start
